package pe.edu.upeu.microservicios1.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.edu.upeu.microservicios1.models.entity.Alumno;

public interface AlumnoRepository extends JpaRepository<Alumno, Long> {

}

