package pe.edu.upeu.microservicios1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microservios1Application {

	public static void main(String[] args) {
		SpringApplication.run(Microservios1Application.class, args);
	}

}
